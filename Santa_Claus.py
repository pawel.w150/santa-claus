# creat a class Character
class Character:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __str__(self):
        return f"{self.name} is {self.age} years old"

    def birthday(self):
        self.age = self.age + 1

# creat a class Building
class Building:
    def __init__(self, name, adress):
        self.name = name
        self.adress = adress

    def __str__(self):
        return f"This is {self.name} Which is located under the adress: {self.adress}"

    def features(self, number_of_workers, manageres, product):
        return f"{self.name} which produce: {product}, employ: {number_of_workers} workers and {manageres} manageres"

# create a class SantaClaus that inherits from a class Character
class SantaClaus(Character):
    species = "Santa Claus"
    
    def work(self):
        print("I am the boss and I'm the one who decides")

# create a Reindeer class that inherits from a class Character
class Reindeer(Character):
    species = "Reinder"

    def work(self):
        print("I'm the reindeer pulling Santa's sleigh")

# create a class Elf that inherits from a class Character
class Elf(Character):
    species = "Elf"

    def work(self):
        print("I'm an Elf who helps St. Santa")

# create a classes Factory, PostOffice and ProjectOffice
# This classes inherits from a class Building
class Factory(Building):
    pass

class PostOffice(Building):
    pass

class ProjectOffice(Building):
    pass

# The main function creates an instance of character objects and buildings
def main():
    SanCla = SantaClaus("SantaClaus", 68)
    Reinder = Reindeer("Rudolf", 15)
    Elmo = Elf("Elmo", 7)

    Production = Factory("Production", "Santa Claus village 56")
    Post = PostOffice("PostOffice", "Santa Claus village 50")
    Project = ProjectOffice("Project Offfice", "Santa Claus village 46")


    print(SanCla)
    print(Reinder)
    print(Elmo)
    print(Production)
    print(Post)
    print(Project)

#calling the main function
if __name__ == "__main__":
    main()
